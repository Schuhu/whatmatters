package de.whatmatters.user;

import javax.validation.constraints.NotEmpty;

public class RegistrationDTO {


    @NotEmpty
    private String name;
    @NotEmpty
    private String password1;
    @NotEmpty
    private String password2;

    public RegistrationDTO(String name, String password1, String password2) {
        this.name = name;
        this.password1 = password1;
        this.password2 = password2;
    }

    public String getName() {
        return name;
    }

    public String getPassword1() {
        return password1;
    }

    public String getPassword2() {
        return password2;
    }

}
