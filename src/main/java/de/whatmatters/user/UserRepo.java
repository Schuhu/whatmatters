package de.whatmatters.user;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends CrudRepository<User, Integer> {

    Optional<User> findByNameAndPassword(String name, String password);

    boolean existsByName(String name);

    List<User> findAll();

    User findById(int id);
}
