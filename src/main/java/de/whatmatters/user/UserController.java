package de.whatmatters.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public String registerUser(Model model) {
        model.addAttribute("registrationDTO", new RegistrationDTO("", "", ""));
        return "registrationTmpl";
    }

    @PostMapping("/registration")
    public String addUser(@Valid @ModelAttribute RegistrationDTO registrationDTO, BindingResult bindingResult) {

        return userService.register(registrationDTO, bindingResult);
    }

    @GetMapping("/admin")
    public String manageUsers(Model model){
        model.addAttribute("users", userService.getUsers());
        return "adminTmpl";
    }

    @PostMapping("/addAdmin/{idString}")
    public String promoteUserAdmin(@PathVariable String idString, Model model){
        int id = Integer.parseInt(idString);
        userService.promoteUserAdmin(id);
        return "redirect:/admin";
    }

    @PostMapping("/addModerator/{idString}")
    public String promoteUserModerator(@PathVariable String idString, Model model){
        int id = Integer.parseInt(idString);
        userService.promoteUserModerator(id);
        return "redirect:/admin";
    }
}
