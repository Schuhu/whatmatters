package de.whatmatters.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.List;

@Service
public class UserService {

    private UserRepo userRepo;

    @Autowired
    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public String register(RegistrationDTO registrationDTO, BindingResult bindingResult) {

        if(!registrationDTO.getPassword1().equals(registrationDTO.getPassword2())){
            bindingResult.addError(new FieldError("user", "password2", "Passwörter stimmen nicht überein."));
        }
        if(userRepo.existsByName(registrationDTO.getName())){
            bindingResult.addError(new FieldError("user", "name", "Benutzer existiert bereits."));
        }
        if(bindingResult.hasErrors()){
            return "registrationTmpl";
        }

        User user = new User(registrationDTO.getName(), registrationDTO.getPassword1());
        userRepo.save(user);
        return "registrationSuccessTmpl";

    }

    public List<User> getUsers() {
        return userRepo.findAll();
    }

    public void promoteUserAdmin(int id) {
        User user = userRepo.findById(id);
        user.setRole((byte)1);
        userRepo.save(user);
    }

    public void promoteUserModerator(int id) {
        User user = userRepo.findById(id);
        user.setRole((byte)2);
        userRepo.save(user);
    }

}
