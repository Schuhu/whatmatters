package de.whatmatters.user;

import de.whatmatters.blog.Comment;
import de.whatmatters.blog.Entry;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private String password;
    private byte role;

    @OneToMany(mappedBy = "user")
    private List<Entry> entries;

    @OneToMany(mappedBy = "user")
    private List<Comment> comments;

    public User(){
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
        this.role = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte getRole() { return role; }

    public int getId() {
        return id;
    }

    public void setRole(byte role) {
        this.role = role;
    }
}
