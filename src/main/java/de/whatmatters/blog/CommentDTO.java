package de.whatmatters.blog;

import javax.validation.constraints.NotEmpty;

public class CommentDTO {

    @NotEmpty
    private String text;

    public CommentDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
