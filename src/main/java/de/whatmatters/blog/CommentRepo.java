package de.whatmatters.blog;

import org.springframework.data.repository.CrudRepository;
import de.whatmatters.user.User;

import java.util.List;

public interface CommentRepo extends CrudRepository<Comment, Integer> {

    List<Comment> findAllByUser(User user);
    List<Comment> findByEntry(Entry entry);

}
