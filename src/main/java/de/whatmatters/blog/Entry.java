package de.whatmatters.blog;

import de.whatmatters.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Entry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty
    private String title;

    @NotEmpty
    @Column(columnDefinition = "TEXT")
    private String text;

    @ManyToMany
    @JoinTable(
            name = "Entry_EntryCategory",
            joinColumns = { @JoinColumn(name = "entry_id")},
            inverseJoinColumns = { @JoinColumn(name = "entryCategory_id")})
    private List<EntryCategory> entryCategories;

    @OneToMany(mappedBy = "entry")
    private List<Comment> comments;

    @ManyToOne
    private User user;

    public Entry() {
    }

    public Entry(String title, String text, User user, String[] entryCategory, List<EntryCategory> oldEntryCategories) {
        this.title = title;
        this.text = text;
        this.user = user;

        List<EntryCategory> newEntryCategories = new ArrayList<>();

        for (int i = 0; i < oldEntryCategories.size(); i++) {
            for (int j = 0; j < entryCategory.length; j++) {
                if(oldEntryCategories.get(i).getCategoryName().equals(entryCategory[j])){
                    newEntryCategories.add(oldEntryCategories.get(i));
                }
            }

        }
        this.entryCategories = newEntryCategories;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public List<EntryCategory> getEntryCategories() {
        return entryCategories;
    }

}
