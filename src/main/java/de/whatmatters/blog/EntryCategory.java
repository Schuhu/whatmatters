package de.whatmatters.blog;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
public class EntryCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String categoryName;

    @ManyToMany(mappedBy = "entryCategories")
    private Set<Entry> entries = new HashSet<>();

    public EntryCategory() {
    }

    public EntryCategory(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getId() {
        return id;
    }

    public Set<Entry> getEntries() {
        return entries;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setEntries(Entry entry) {
        this.entries.add(entry);
    }
}
