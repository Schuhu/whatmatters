package de.whatmatters.blog;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EntryCategoryRepo extends CrudRepository<EntryCategory, Integer> {

    List<EntryCategory> findAll();
    EntryCategory findByCategoryName(String entryCategoryName);
}
