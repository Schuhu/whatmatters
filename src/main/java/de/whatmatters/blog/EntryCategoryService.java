package de.whatmatters.blog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EntryCategoryService {

    private EntryCategoryRepo entryCategoryRepo;

    @Autowired
    public EntryCategoryService(EntryCategoryRepo entryCategoryRepo) {
        this.entryCategoryRepo = entryCategoryRepo;
    }

    public void addEntryCategory(EntryCategory entryCategory) {
        entryCategoryRepo.save(entryCategory);
    }

    //Entries werden in entsprechender Kategorie gespeichert
    public void setEntry(Entry entry) {
        for (int i = 0; i < entry.getEntryCategories().size(); i++) {
            EntryCategory entryCategory = entry.getEntryCategories().get(i);
            entryCategory.setEntries(entry);
            entryCategoryRepo.save(entryCategory);
        }
    }

    public List<EntryCategory> getEntryCategories(){
        return entryCategoryRepo.findAll();
    }

    public List<EntryCategory> selectCategory(EntryCategoryDTO entryCategoryDTO) {

        if(entryCategoryDTO.getEntryCategory().equals("Kategorie")){
            return entryCategoryRepo.findAll();
        }

        List<EntryCategory> entryCategories = new ArrayList<>();
        entryCategories.add(entryCategoryRepo.findByCategoryName(entryCategoryDTO.getEntryCategory()));
        return entryCategories;
    }
}
