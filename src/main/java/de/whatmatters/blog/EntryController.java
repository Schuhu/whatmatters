package de.whatmatters.blog;

import de.whatmatters.session.LoginDTO;
import de.whatmatters.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;


@Controller
public class EntryController {

    private EntryCommentService entryCommentService;
    private EntryCategoryService entryCategoryService;

    @Autowired
    public EntryController(EntryCommentService entryCommentService, EntryCategoryService entryCategoryService) {
        this.entryCommentService = entryCommentService;
        this.entryCategoryService = entryCategoryService;
    }

    @GetMapping("/blog")
    public String index(Model model) {

        model.addAttribute("entries", entryCommentService.getEntries());
        model.addAttribute("entryCategories", entryCategoryService.getEntryCategories());
        model.addAttribute("loginDTO", new LoginDTO("", ""));
        model.addAttribute("entryCategoryDTO", new EntryCategoryDTO("Kategorien"));
        return "blogTmpl";
    }

    @PostMapping("/blog")
    public String index(@ModelAttribute("sessionUser") User sessionUser, Model model, @ModelAttribute EntryCategoryDTO entryCategoryDTO) {

        model.addAttribute("entries", entryCommentService.getEntries());
        model.addAttribute("entryCategories", entryCategoryService.selectCategory(entryCategoryDTO));
        model.addAttribute("loginDTO", new LoginDTO("", ""));
        model.addAttribute("entryCategoryDTO", new EntryCategoryDTO("Kategorien"));

        return "blogTmpl";
    }

    @PostMapping("/addComment/{idString}")
    public String addComment(@ModelAttribute("sessionUser") User sessionUser, @Valid @ModelAttribute("commentDTO") CommentDTO commentDTO, BindingResult bindingResult, @PathVariable String idString, Model model) {

        int id = Integer.parseInt(idString);
        if(bindingResult.hasErrors()){
            model.addAttribute("entry", entryCommentService.getEntry(id));
            return "commentTmpl";
        }
        Comment comment = new Comment(commentDTO.getText(), entryCommentService.getEntry(id), sessionUser);
        entryCommentService.addComment(comment);
        return "redirect:/blog";
    }

    @GetMapping("/comment/{idString}")
    public String writeComment(@ModelAttribute("sessionUser") User sessionUser, @PathVariable String idString, Model model) {
        int id = Integer.parseInt(idString);
        model.addAttribute("entry", entryCommentService.getEntry(id));
        model.addAttribute("commentDTO", new CommentDTO(""));
        return "commentTmpl";
    }

    @GetMapping("/entry")
    public String writeEntry(@ModelAttribute("sessionUser") User sessionUser, Model model) {
        model.addAttribute("entryDTO", new EntryDTO("", ""));
        model.addAttribute("entryCategories", entryCategoryService.getEntryCategories());
        return "entryTmpl";
    }

    @PostMapping("/addEntry")
    public String addEntry(@ModelAttribute("sessionUser") User sessionUser, @Valid  @ModelAttribute("entryDTO") EntryDTO entryDTO, BindingResult bindingResult, Model model) {

        if(bindingResult.hasErrors())
        {model.addAttribute("entryCategories", entryCategoryService.getEntryCategories());
            return "entryTmpl";
        }
        Entry entry = new Entry(entryDTO.getTitle(), entryDTO.getText(), sessionUser, entryDTO.getEntryCategory(), entryCategoryService.getEntryCategories());
        entryCommentService.addEntry(entry);
        entryCategoryService.setEntry(entry);
        return "redirect:/blog";
    }

    @GetMapping("/manageEntry/{idString}")
    public String manageEntry(@ModelAttribute("sessionUser") User sessionUser, @PathVariable String idString, Model model) {
        int id = Integer.parseInt(idString);
        model.addAttribute("entry", entryCommentService.getEntry(id));
        return "manageEntryTmpl";
    }

    @PostMapping("/deleteCommentAdmin/{idString}/{entryIdString}")
    public String deleteCommentAdmin(@PathVariable String idString, @PathVariable String entryIdString) {
        int id = Integer.parseInt(idString);
        entryCommentService.deleteComment(id);
        return "redirect:/manageEntry/" + entryIdString;
    }

    @PostMapping("/editEntry/{idString}")
    public String editEntry(@ModelAttribute("sessionUser") User sessionUser, @PathVariable String idString, Model model) {
        int id = Integer.parseInt(idString);
        model.addAttribute("entry", entryCommentService.getEntry(id));
        return "editEntryTmpl";
    }

    @PostMapping("/editEntrySuccess/{idString}")
    public String editEntrySuccess(@ModelAttribute("entry") Entry entry, @PathVariable String idString, @ModelAttribute("sessionUser") User sessionUser) {
        int id = Integer.parseInt(idString);
        entryCommentService.editEntry(entry, id, sessionUser);
        return "redirect:/blog";
    }
    @PostMapping("/deleteEntry/{idString}")
    public String deleteEntry(@PathVariable String idString, @ModelAttribute("sessionUser") User sessionUser) {
        int id = Integer.parseInt(idString);
        entryCommentService.deleteAllCommentsByEntry(entryCommentService.getEntry(id));
        entryCommentService.deleteEntry(id);
        return "redirect:/blog";
    }
}
