package de.whatmatters.blog;

import de.whatmatters.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CommentController {

    private EntryCommentService entryCommentService;

    @Autowired
    public CommentController(EntryCommentService entryCommentService) {
        this.entryCommentService = entryCommentService;
    }

    @GetMapping("/manageComments")
    public String manageComments(@ModelAttribute("sessionUser") User sessionUser, Model model) {
        model.addAttribute("comments", entryCommentService.getComments(sessionUser));
        return "manageCommentsTmpl";
    }

    @PostMapping("/deleteComment/{idString}")
    public String deleteComment(@PathVariable String idString) {
        int id = Integer.parseInt(idString);
        entryCommentService.deleteComment(id);
        return "redirect:/manageComments";
    }

}
