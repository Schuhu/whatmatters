package de.whatmatters.blog;

import de.whatmatters.user.User;

import javax.persistence.*;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(columnDefinition = "TEXT")
    private String text;

    @ManyToOne
    private Entry entry;

    @ManyToOne
    private User user;

    public Comment() {
    }

    public Comment(String text, Entry entry, User user) {
        this.text = text;
        this.entry = entry;
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }
}
