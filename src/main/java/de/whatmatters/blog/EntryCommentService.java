package de.whatmatters.blog;

import de.whatmatters.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntryCommentService {

    private EntryRepo entryRepo;
    private CommentRepo commentRepo;


    @Autowired
    public EntryCommentService(EntryRepo entryRepo, CommentRepo commentRepo) {
        this.entryRepo = entryRepo;
        this.commentRepo = commentRepo;

    }

    public List<Entry> getEntries(){
        return entryRepo.findAllByOrderByIdDesc();
    }


    public void addComment(Comment comment) {
        commentRepo.save(comment);
    }


    public Entry getEntry(int id) {
       return entryRepo.findById(id).get();
    }

    public void addEntry(Entry entry) {

        entryRepo.save(entry);
    }

    public List<Comment> getComments(User user) {
        return commentRepo.findAllByUser(user);
    }

    public void deleteAllCommentsByEntry(Entry entry) {
        List<Comment> comments = commentRepo.findByEntry(entry);
        for (int i = 0; i < comments.size(); i++) {
            commentRepo.delete(comments.get(i));
        }
    }

    public void deleteComment(int id) {
        commentRepo.deleteById(id);
    }

    public void deleteEntry(int id) {
        entryRepo.deleteById(id);
    }

    public void editEntry(Entry entry, int id, User user) {
        Entry newEntry = entryRepo.findById(id).get();
        newEntry.setText(entry.getText());
        newEntry.setTitle(entry.getTitle());
        newEntry.setUser(user);
        entryRepo.save(newEntry);
    }


}
