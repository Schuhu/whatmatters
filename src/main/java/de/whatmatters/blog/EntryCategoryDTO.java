package de.whatmatters.blog;

public class EntryCategoryDTO {

    private String entryCategory;

    public EntryCategoryDTO(String entryCategory) {
        this.entryCategory = entryCategory;
    }

    public String getEntryCategory() {
        return entryCategory;
    }

}
