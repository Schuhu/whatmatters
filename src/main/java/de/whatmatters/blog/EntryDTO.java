package de.whatmatters.blog;

import javax.validation.constraints.NotEmpty;

public class EntryDTO {

    @NotEmpty
    private String text;

    @NotEmpty
    private String title;

    private String[] entryCategory;

    public EntryDTO(String text, String title) {
        this.text = text;
        this.title = title;
        this.entryCategory = new String[3];
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public String[] getEntryCategory() {
        return entryCategory;
    }
}
