package de.whatmatters.blog;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface EntryRepo extends CrudRepository<Entry, Integer> {

    List<Entry> findAllByOrderByIdDesc();
    Optional<Entry> findById(int id);

}
