package de.whatmatters.session;

import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.Optional;

public interface SessionRepo extends CrudRepository<Session, Integer> {

    Optional<Session> findByIdAndExpiresAtAfter(String id, Instant now);
}
