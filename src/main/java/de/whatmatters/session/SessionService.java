package de.whatmatters.session;

import de.whatmatters.user.User;
import de.whatmatters.user.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Optional;

@Service
public class SessionService {

    SessionRepo sessionRepo;
    UserRepo userRepo;

    @Autowired
    public SessionService(SessionRepo sessionRepo, UserRepo userRepo) {
        this.sessionRepo = sessionRepo;
        this.userRepo = userRepo;
    }


    public String login(LoginDTO loginDTO, HttpServletResponse response, BindingResult bindingResult) {

        Optional<User> optionalUser = userRepo.findByNameAndPassword(loginDTO.getName(), loginDTO.getPassword());

        if (optionalUser.isPresent()) {
            Session session = new Session(optionalUser.get(), Instant.now().plusSeconds(7*24*60*60));
            sessionRepo.save(session);

            Cookie cookie = new Cookie("sessionId", session.getId());
            response.addCookie(cookie);

        } else {

            bindingResult.addError(new FieldError("loginDTO", "password", "Login fehlgeschlagen."));
            return "blogTmpl";
        }


        return "redirect:/blog";
    }

    public void logout(HttpServletResponse response, String sessionId) {
        Optional<Session> optionalSession = sessionRepo.findByIdAndExpiresAtAfter(sessionId, Instant.now());
        optionalSession.ifPresent(session -> sessionRepo.delete(session));

        Cookie cookie = new Cookie("sessionId", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }
}
